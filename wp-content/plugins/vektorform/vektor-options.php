<?php

/*
 * Create our admin menu
 */

function vektor_form_create_menu() {

	// create new menu item
	$page = add_options_page(__('Contact form', 'vektorform'), __('Contact form', 'vektorform'), 'administrator', 'vektor-form', 'vektor_form_settings_page');
	
	// call register settings function
	add_action( 'admin_init', 'vektor_form_admin_init' );
	
}

// create custom plugin settings menu
add_action('admin_menu', 'vektor_form_create_menu');

/*
 * Init admin settings
 */

function vektor_form_admin_init() {

	// register our settings
	register_setting( 'vektor-form-settings-group', 'vektor_form_email' );
}

function vektor_form_settings_page() {
?>
<div class="wrap">
	<h2><?php echo get_admin_page_title(); ?></h2>
	<form method="post" action="options.php" class="vektor-form">
		<?php settings_fields( 'vektor-form-settings-group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><?php _e('Email', 'vektorform'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_form_email" value="<?php echo get_option('vektor_form_email'); ?>" /></td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
	</form>
</div> <!-- /.wrap -->
<?php } ?>