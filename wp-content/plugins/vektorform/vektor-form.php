<?php

/*
Plugin Name:		Vektor form
Plugin URI:			http://www.vektorgrafik.se
Description:		Use the shortcode [vektor_form] to display a form on a page / post. Email has to be set under Settings > Vektor form.
Version:			1.0
Author:				Vektorgrafik Stockholm AB
Author URI:			http://www.vektorgrafik.se
*/

if(is_admin()){
	include_once('vektor-options.php');
}

/*
 * Form
 */

function vektor_form(){
ob_start();
?>
<div class="row">
	<form action="<?php echo vektor_get_current_url(); ?>" class="vektor-form" method="post">
		<div class="medium-12 columns">
		<p class="status"></p>
		</div> <!-- /.medium-12 -->
		<div class="medium-4 columns">
			<input type="text" class="name" name="name" placeholder="<?php _e('Name', 'vektorform'); ?>*">
		</div> <!-- /.medium-4 -->
		<div class="medium-4 columns">
			<input type="text" class="email" name="email" placeholder="<?php _e('Email', 'vektorform'); ?>*">
		</div> <!-- /.medium-4 -->
		<div class="medium-4 columns">
			<input type="text" class="phone" name="phone" placeholder="<?php _e('Phone', 'vektorform'); ?>">
		</div> <!-- /.medium-4 -->
		<div class="medium-8 columns">
			<textarea class="message" name="message" placeholder="<?php _e('Message..', 'vektorform'); ?>*"></textarea>
		</div> <!-- /.medium-8 -->
		<div class="medium-4 columns">
			<input type="submit" class="submit" name="submit" value="<?php _e('Send', 'vektorform'); ?>">
		</div> <!-- /.medium-4 -->
		<input type="hidden" name="action" value="vektor_form_ajax">
		<?php wp_nonce_field( 'vektor-form-nonce', 'security' ); ?>
	</form>
</div> <!-- /.row -->
<?php
$html = ob_get_clean();
return $html;
}

add_shortcode('vektor_form', 'vektor_form');

/*
 * Ajax
 */

function vektor_form_ajax(){
	
	// First check the nonce, if it fails the function will break
	check_ajax_referer('vektor-form-nonce', 'security');
	
	// Get sitename
	$sitename = get_option('vektor_name') ? get_option('vektor_name') : get_bloginfo('name');
	
	// Get site url
	$siteurl = get_site_url();

	// Nonce is checked, get the POST data and send the mail
	$data = array();

	$data['name'] = trim($_POST['name']);
	$data['email'] = trim($_POST['email']);
	$data['phone'] = trim($_POST['phone']);
	$data['message'] = trim($_POST['message']);

	$data['subject'] = "Kontaktformulär: " . $data['name'];

	// Send the mail to this email
	$to = get_option('vektor_form_email') ? get_option('vektor_form_email') : "alexander@vektorgrafik.se";
	
	// Headers
	$headers = "From: " . $sitename . " <" . trim($data['email']) . ">\r\n";
	$headers .= "Reply-To: ". trim($data['email']) . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=utf-8\r\n";
	
	$message = "<html><body>";
	
	// Set the message that should be sent
	$message .= __("Name", 'vektorform') . ": " . $data['name'] . " <br />";
	$message .= __("Email", 'vektorform') . ": " . $data['email'] . "<br />";
	$message .= __("Phone", 'vektorform') . ": " . $data['phone'] . "<br />";
	$message .= "\n" . __("Message", 'vektorform') . ": " . $data['message'];
	
	$message .= "</html></body>";
	
	// Send mail
	if($to){
		$mail = wp_mail($to, $data['subject'], $message, $headers);
	} else {
		$mail = false;
	}

	// Check if mail was sent
	if($mail){
		echo json_encode(array(
			'status' => true,
			'message'=> __('Thanks for your message. We will get back to you shortly.', 'vektorform')
		));
	} else {
		echo json_encode(array(
			'status' => false,
			'message'=> __('Something went wrong. Please try again.', 'vektorform')
		));
	}
	

	die();

}

/*
 * Enqueue scripts
 */

function vektor_form_scripts(){

	// JQuery plugins
	wp_enqueue_script('placeholders', plugins_url( '/js/placeholders.min.js', __FILE__ ), array('jquery'), null, true );
	wp_enqueue_script('validation', plugins_url( '/js/validation.min.js', __FILE__ ), array('jquery'), null, true );

	// Scripts
	wp_enqueue_script('vektor-form-script', plugins_url( '/js/vektor-form.js', __FILE__ ), array('jquery'), null, true );

	wp_localize_script( 'vektor-form-script', 'vektor_form_object', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'redirecturl' => $_SERVER['REQUEST_URI'],
		'loadingmessage' => __('Var god vänta...')
	));

}

/*
 * Translation
 */

function vektor_form_lang(){
	load_plugin_textdomain('vektorform', false, dirname(plugin_basename(__FILE__ )) . '/lang/');
}

add_action('plugins_loaded', 'vektor_form_lang');

/*
 * Get current url
 */

function vektor_get_current_url($with_query_string = false) {
	$protocol = 'http';
	if(!empty($_SERVER['HTTPS']))
		$protocol .= 's';
	
	$protocol .= '://';
	
	$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	if(!$with_query_string)
		$url = str_replace($_SERVER['QUERY_STRING'], '', $url);
	
	return trim($url, '?');
}

/*
 * Init vektor form
 */

function vektor_form_init(){

	// Enqueue scripts
	add_action( 'wp_enqueue_scripts', 'vektor_form_scripts' );

	// Enable the user WITH privileges to run vektor_ajax_form() in AJAX
	add_action( 'wp_ajax_vektor_form_ajax', 'vektor_form_ajax' );

	// Enable the user WITHOUT privileges to run vektor_ajax_form() in AJAX
	add_action( 'wp_ajax_nopriv_vektor_form_ajax', 'vektor_form_ajax' );

}

add_action('init', 'vektor_form_init');