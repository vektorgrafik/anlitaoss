var vektor_form = {
	
	init: function(){
			
		// Divs
		var $form = $('.vektor-form'),
			$status = $('.status');
		
		// Hide status message
		$status.hide();
		
		$form.each(function(index, Element){
			
			// Validate form
			$(Element).validate({
				rules: {
					name: {
						required: true
					},
					email: {
						required: true,
						email: true
					},
					message: {
						required: true,
						minlength: 3
					}
				},
				messages: {
					name: "Förnamn är obligatoriskt.",
					email: "Var god ange en giltig e-postadress.",
					message: {
						required: "Det här fältet är obligatoriskt.",
						minlength: "Ange minst 3 bokstäver."
					}
				},
				submitHandler: function(){
					
					// serialize form data
					var formData = $(Element).serialize();
					
					// show loading message
					$status.show().text(vektor_form_object.loadingmessage);
					
					// disable inputs and textareas
					$(Element).find('input, textarea').attr('disabled', 'disabled');
					
					$.ajax({
						 type: 'POST',
						 dataType: 'json',
						 url: vektor_form_object.ajaxurl,
						 data: formData,
						 success: function(data){
						 	console.log(data);
						 	// show status message
							$status.text(data.message);
							if(data.status == true){
								// add success class
								$status.addClass('success');
								// remove disabled and clear inputs and textareas
								$(Element).find('input, textarea').attr('disabled', false).not('[type="submit"]').val('');
							} else {
								// add error class
								$status.addClass('error');
							}
						 }
					 }); // end ajax
					 
				 } // end submitHandler();
				 
			}); // end $(Element).validate();
			
		}); // end $form.each();

	} // init
	
}

$(document).ready(function(){
	vektor_form.init();
}); // document ready