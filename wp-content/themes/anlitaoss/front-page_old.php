<?php
	get_header();

	get_template_part('templates/frontpage/top-section');
	get_template_part('templates/frontpage/form-choose-main-service');
	get_template_part('templates/frontpage/form-list-results');
	get_template_part('templates/frontpage/about');
	
	get_footer();
?>