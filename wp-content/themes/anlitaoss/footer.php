<?php
/*$vektor_name = get_option('vektor_name');
$vektor_email = get_option('vektor_email');
$vektor_phone = get_option('vektor_phone');
$vektor_address = get_option('vektor_address');
$vektor_zip = get_option('vektor_zip');
$vektor_city = get_option('vektor_city');
$vektor_country = get_option('vektor_country');
$vektor_open = get_option('vektor_open');*/
?>		

		</div> <!-- /#content -->
		<?php if( ! isset( $_GET['content-only'] )) : ?>
		<footer id="footer">
		
			<div id="bottom">
			
				<div class="row">
				
					<div class="small-12 columns text-right">
					
						<small><?=get_option( 'vektor_footer_text' ); ?></small>
						
					</div> <!-- /.medium-6 -->
					
				</div> <!-- /.row -->
				
			</div> <!-- /#bottom -->
			
		</footer>
		<?php endif; ?>
		
		<?php wp_footer(); ?>
		
	</body>
	
</html>