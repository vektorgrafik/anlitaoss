<?php get_header(); ?>
<?php the_post(); ?>

	<?php
	if( ! isset( $_GET['content-only'] ))
		get_template_part('templates/page/page-header');
	?>
	
	<?php get_template_part('templates/page/page-content'); ?>

<?php get_footer(); ?>