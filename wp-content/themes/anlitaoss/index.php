<?php get_header(); ?>
	
	<section class="article-content">
	
		<div class="row">
			
			<div class="medium-12 columns">
				
				<?php get_search_form(); ?>
				
				<?php while (have_posts()) : the_post(); ?>

					<article <?php post_class(); ?>>
						
						<time class="time" datetime="<?php echo get_the_date('Y-m-d H:i'); ?>"><?php echo get_the_date('Y-m-d'); ?></time>
						
						<h2 class="h4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<?php the_excerpt(); ?>
					
					</article>
					
				<?php endwhile; ?>
				
				<?php wp_reset_query(); ?>
			
			</div> <!-- /.medium-12 -->
			
			<?php if ( function_exists( 'vektor_page_navi' ) ) { ?>
			
				<nav class="pagination medium-12 columns">
				
					<?php vektor_page_navi(); ?>
					
				</nav>
				
			<?php } else { ?>
			
				<nav class="wp-prev-next">
				
					<ul class="clearfix">
					
						<li class="prev-link">
						
							<?php next_posts_link( __( '&larr; Older posts', 'vektor' )) ?>
							
						</li>
						
						<li class="next-link">
						
							<?php previous_posts_link( __( 'Newer posts &rarr;', 'vektor' )) ?>
							
						</li>
					</ul>
					
				</nav>
				
			<?php } ?>
			
		</div> <!-- /.row -->
		
	</section>
	
<?php get_footer(); ?>