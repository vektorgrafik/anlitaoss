<?php
/*
 * Template Name: Om oss
 */


get_header();
the_post();

	get_template_part('templates/page/page-header');
	get_template_part('templates/page/page-about');

get_footer();

?>