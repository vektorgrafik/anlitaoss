<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<title><?php wp_title('-', true, 'right'); ?></title>
		

		<link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri() . '/lib/img/favicons/'?>/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="<?=get_template_directory_uri() . '/lib/img/favicons/'?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?=get_template_directory_uri() . '/lib/img/favicons/'?>/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?=get_template_directory_uri() . '/lib/img/favicons/'?>/manifest.json">
		<link rel="mask-icon" href="<?=get_template_directory_uri() . '/lib/img/favicons/'?>/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
		<meta name="format-detection" content="telephone=no"/>

		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBKyZtuo1WDjohf5zAuM-3-77070TFaZyA&v=3.11" type="text/javascript"></script>
		<script src="https://use.typekit.net/ive7dhw.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
	
		<?php wp_head(); ?>
		
		<?php if( !is_localhost() && !is_dev() ) : ?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?= get_option( 'vektor_google_tag_manager' ); ?>');</script>
		<!-- End Google Tag Manager -->
		<?php endif; ?>

	</head>
	<body <?php body_class(); ?>>
	
	<?php if( !is_localhost() && !is_dev() ) : ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?=get_option('vektor_google_tag_manager'); ?>"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php endif; ?>

	<div id="main-preloader"></div>
	<!--[if lt IE 9]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
	
	<?php if( ! isset( $_GET['content-only'])) : ?>
	<header id="header">
		
		<div class="row">

			<div class="medium-12 columns">
				<a href="<?php echo home_url(); ?>" id="logo"><?php bloginfo('name'); ?></a>
				<div id="toggle">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<nav id="nav">
					<?php vektor_nav(); ?>
				</nav>
			</div> <!-- /.medium-12 -->
		</div> <!-- /.row -->
		
	</header>	
	<?php endif;?>

	<div id="content">