<?php

	/* Template name: Portal-sida */

	the_post();

	get_header();

	get_template_part('templates/portal/top-section');
	get_template_part('templates/portal/info');
	get_template_part('templates/portal/about');
	
	get_footer();
?>