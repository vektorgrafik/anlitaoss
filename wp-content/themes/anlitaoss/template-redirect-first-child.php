<?php
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			$pages = get_pages("child_of=".$post->ID."&sort_column=menu_order");
			$firstchild = $pages[0];
			wp_redirect(get_permalink($firstchild->ID));
		}
	}
?>