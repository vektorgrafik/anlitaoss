<?php

get_header();
the_post();

$data = get_field_objects();

$org_nr = $data["organization_number"]["value"] ?? false;
$visiting_address = $data["visiting_address"]["value"] ?? false;
$postal_address = $data["postal_address"]["value"] ?? false;
$area = $data["area"]["value"] ?? false;
$phone_number = $data["phone_number"]["value"] ?? false;
$contact_person = $data["contact_person"]["value"] ?? false;
$email = $data["email"]["value"] ?? false;
$website = $data["website"]["value"] ?? false;
$logo = $data["logo"]["value"] ?? false;

$lat = $data['visiting_address']['value']['lat'];
$lng = $data['visiting_address']['value']['lng'];

$company_presentation_title = $data["company_presentation_title"]["value"] ?? false;
$company_presentation = $data["company_presentation"]["value"] ?? false;
$company_preamble = $data["company_presentation_preamble"]["value"] ?? false;
$company_image = $data["company_image"]["value"] ?? false;
$company_top_image = $data["company_top_section_image"]["value"] ?? false;

$company_image_repeater = $data["company_image_repeater"]["value"] ?? false;

if ( $company_top_image ) {
	$top_section_image = 'style=" background-image: url(' . $company_top_image["url"] . ')"';
} else {
	$top_section_image = '';
}

?>

	<?php if ( $company_top_image ) : ?>

		<section class="top-section top-section--company" <?=$top_section_image ;?> >
		
			<div class="row">
				<div class="medium-12 columns">			
					
				</div> <!-- .columns -->
			</div> <!-- end .row -->
			
		</section>

	<?php endif; ?>

	<input type="hidden" name="company_lat" id="company_lat" value="<?=$lat?>">
	<input type="hidden" name="company_lng" id="company_lng" value="<?=$lng?>">
	<input type="hidden" name="company_name" id="company_name" value="<?=htmlentities( get_the_title() )?>">

	<section class="company-content-header">
		<div class="row">

			<div class="small-12 medium-7 columns">
			
				<h1 class="h2 company-content-header__title"><?php the_title(); ?></h1>

			</div>  <!-- end .column -->

			<div class="small-12 medium-5 xlarge-4 xlarge-push-1 columns align-right">
			
				<ul class="list list--horizontal list--menu">

					<li class="list__item"><a href="#company-about" class="scroll-to"><?php _e('Om företaget', 'vektor'); ?></a></li>
					
					<?php if ( $company_image_repeater ): ?>
						<li class="list__item"><a href="#company-images" class="scroll-to"><?php _e('Referensbilder', 'vektor'); ?></a></li>
					<?php endif; ?>

					<li class="list__item"><a href="#company-map" class="scroll-to"><?php _e('Karta', 'vektor'); ?></a></li>

				</ul>

			</div>  <!-- end .column -->

		</div> <!-- end .row -->
	</section>

	<section id="company-about" class="company-content space">

		<div class="row">

			<div class="column large-7">
				<?php if ( $company_presentation_title ) {
					echo '<h2 class="body-font h3 company-title strong">' .  $company_presentation_title . '</h2>';
				} ?>

				<?php if ( $company_preamble ) {
					echo '<span class="preamble">';
					echo $company_preamble;
					echo '</span>';
				} ?>

				<?php if ( $company_presentation ) {
					echo $company_presentation;
				} ?>
			</div>

			<div class="column large-5 xlarge-4 xlarge-push-1">

				<?php if( $logo ) : ?>
				<div class="company-logo">
					<img src="<?=$logo['url']?>" title="<?=$logo['title']?>" alt="<?=$logo['title']?>">
				</div>

				<?php endif; ?>

				<div class="contact-block box">

					<h3 class="company-content__title h6 body-font strong"><?php _e('Kontaktuppgifter', 'vektor'); ?></h3>
					
					<ul class="list list--large-contact">
						<?php if ( $contact_person ) : ?>
							<li class="list__item list__item--contact-person">
								<span class="list__item-title strong"><?php _e('Kontaktperson: ', 'vektor'); ?></span>
								<?php echo $contact_person; ?>
							</li>
						<?php endif; ?>

						<?php if ( $phone_number  ) : ?>
							<li class="list__item list__item--phone">
								<span class="list__item-title strong"><?php _e('Telefon: ', 'vektor'); ?></span>
								<?php echo $phone_number ; ?>
							</li>
						<?php endif; ?>

						<?php if ( $email  ) : ?>
							<li class="list__item list__item--mail">
								<span class="list__item-title strong"><?php _e('E-post: ', 'vektor'); ?></span>
								<a href="mailto:<?=$email?>"?><?=$email?></a>
							</li>
						<?php endif; ?>

						<?php if ( $website  ) : ?>
							<li class="list__item list__item--website">
								<span class="list__item-title strong"><?php _e('Hemsida: ', 'vektor'); ?></span>
								<a href="http://<?=$website?>" target="_blank"><?php echo $website ; ?></a>
							</li>
						<?php endif; ?>

					</ul>

				</div> <!-- end .contact-block -->

			</div> <!-- end .column -->
			

		</div> <!-- end .row -->
		
	</section>

	<?php if ( $company_image_repeater ): ?>

		<?php 

			$image_count = count($company_image_repeater);

		?>

		<section id="company-images" class="company-image-section space">

			<div class="row">
				<div class="column small-12">
					<h3 class="company-image-section__title h6 body-font strong"><?php _e('Referensbilder', 'vektor'); ?></h3>
				</div>
			</div> <!-- end .row -->

			<div class="row">		

				<div class="small-12 columns">

				<div class="gallery">
					<div class="gutter-sizer"></div>
					<div class="grid-sizer"></div>
					<?php $i = 1; foreach( $company_image_repeater as $company_image ) : ?>

						<div class="gallery__item"> 
							<div class="gallery__image">
								<img src="<?=$company_image["repeater_image"]["sizes"]["large"];?>" alt="">
							</div>

						</div> <!-- end .gallery__item -->

					<?php $i++; endforeach; ?>
					
					</div>

				</div>

			</div> <!-- end .row -->
			
		</section> <!-- end .company-image-section -->

	<?php endif; ?>

	<section id="company-map" class="company-map-section">
		
		<div id="gmaps-company-map"></div>

	</section>
	
	<?php get_template_part('templates/frontpage/about'); ?>
	
<?php get_footer(); ?>