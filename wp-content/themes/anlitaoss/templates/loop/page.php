<article <?php post_class('post company-post clearfix clickable') ?>>
	
	<div class="row clickable-rows">
	
		<div class="medium-12 columns">
			
			<h2 class="h5"><?php echo the_title(); ?></h2>
	
			<p>
				<?php echo limit_words(get_the_excerpt(), 30); ?>... <a class="readmore" href="<?php the_permalink(); ?>"><?php _e('Continued', 'vektor'); ?> »</a>
			</p>
			
		</div> <!-- /.medium-12 -->	
			
	</div> <!-- /.row -->
	
</article>