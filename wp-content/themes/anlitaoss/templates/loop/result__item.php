<div class="result__item" data-id="<?=$i-1?>">
	
	<?php if( $post->meta['company_image']['value'] ) : ?>
	<div class="result__image" style="background-image:url(<?=$post->meta['company_image']['value']['sizes']['medium_large']?>);"></div>
	<?php endif; ?>

	<div class="result__content">
		
		<h5 class="result__company-heading">
			<span class="result__count"><?=$item_index?>.</span><a href="<?=get_permalink( $post->ID ); ?>"><?=$post->title?></a>

			<?php if( isset( $post->meta['area']['value'] )) : ?>
			<span class="result__area"><?=$post->meta['area']['value']?></span>
			<?php endif; ?>
		</h5>
		
		<ul class="list list--contact-details">
			
			<?php if( $post->meta['contact_person']['value'] ) : ?>
			<li class="list__item list__item--name"><?=$post->meta['contact_person']['value']?></li>
			<?php endif;?>
			
			<?php if( $post->meta['email']['value'] ) : ?>
			<li class="list__item list__item--mail"><a href="mailto:<?=$post->meta['email']['value']?>"><?=$post->meta['email']['value']?></a></li>
			<?php endif; ?>

			<?php if( $post->meta['phone_number']['value'] ) : ?>
			<li class="list__item list__item--phone"><a href="tel:<?=$post->meta['phone_number']['value']?>"><?=$post->meta['phone_number']['value']?></a></li>
			<?php endif; ?>
		</ul>
		<a href="<?=get_permalink( $post->ID ) ?>" class="btn btn-small">Mer info <span class="btn__icon"><i class="icon-arrow-right smaller"></i></span></a>
	</div>
</div> <!-- end .result__item -->