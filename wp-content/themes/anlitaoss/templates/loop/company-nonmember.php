<div <?php post_class('post medium-4 columns non-member inline-block') ?>>

	<div class="non-member-content">
		<h2 class="h6 company-name"><?php the_title(); ?></h2>
		
		<?php 
			$field = get_field_object('factoring_type');
			$value = get_field('factoring_type');
			$label = $field['choices'][ $value ];
		?>
			
		<?php if(get_field('factoring_type')) {?>
				
			<p class="factoring-type strong"><?php echo $label; ?></p>
					
		<?php }?>
		
		<?php if($website = get_field('website')){ ?>
		
			<span class="strong email-label"><?php _e('Website', 'vektor'); ?>:</span>
			<a href="http://<?php echo $website; ?>" target="_blank" class="email"><?php echo $website; ?></a>
			
		<?php } ?>
	
	</div>	
</div>
