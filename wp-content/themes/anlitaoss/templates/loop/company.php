<?php

$btn_text_full_info = get_field('btn_text_full-info', 121);
$btn_text_single_company = get_field('btn_text_single-company', 121);

?>

<article <?php post_class('post company-post clearfix clickable') ?>>
	
	<div class="row">
		
		<div class="medium-2 small-3 columns">
		
			<?php
				$image = get_field('logo');
				$alt = get_the_title($image['id']);
				$image = wp_get_attachment_image_src($image['id'], 'company-logo');
			?>
			
			<div class="logo">
			
				<?php if(get_field('logo')){ ?>				
			
					<img src="<?php echo $image[0]; ?>" title="<?php echo the_title(); ?>" alt="<?php echo $alt; ?>">
				
				<?php } else { ?>
				
				<span class="placeholder"></span>
				
				<?php } ?>
				
			</div> <!-- /.logo -->
			
			
		</div> <!-- /.medium-2 -->
		
		<div class="medium-5 small-9 columns">
		
			<h2 class="h6 company-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<?php 
				$field = get_field_object('factoring_type');
				$value = get_field('factoring_type');
				$label = $field['choices'][ $value ];
			?>
					
			<!--<?php if(get_field('factoring_type')) {?>
						
				<p class="factoring-type strong"><?php echo $label; ?></p>
							
			<?php }?>-->
			
			<a href="<?php the_permalink(); ?>" class="strong read-more-link">Till företagsprofil »</a>
			
		</div> <!-- /. medium-5 -->
		
		<ul class="medium-4 small-12 columns">
		
			<?php if(get_field('factoring_type')) {?>
						
				<li>
					<span class="strong">Factoringtyp: </span>
					<?php echo $label; ?>
				</li>
							
			<?php }?>
		
			<?php if ($website = get_field('website')){ ?>
				<li class="website-link">
					<span class="strong"><?php _e('Website', 'vektor'); ?>:</span>
					<a href="http://<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
				</li>
			<?php } ?>
			
		
		</ul> <!-- /. medium-5 -->
		
	</div><!-- /. row -->
	
	<!--<div class="row">
	
		<div class="medium-4 columns our_services">
		
			<p><span class="strong">Våra tjänster är:</span>
			
			<?php
				$terms = get_the_terms( $post->ID, 'company_service' );
										
				if ( $terms && ! is_wp_error( $terms ) ) : 
				
					$services_links = array();
				
					foreach ( $terms as $term ) {
						$services_links[] = $term->name;
					}
										
					$services = join( ", ", $services_links );
				?>
					<span><?php echo $services; ?></span>
				</p>
				
				<?php endif; ?>
		
		</div> <!-- /.medium-4 -->
		
		<!--<ul class="medium-4 columns companyinfo">
			
			<?php if ($email = get_field('email')){ ?>
				<li>
					<span class="strong"><?php _e('Email', 'vektor'); ?>:</span>
					<?php echo $email; ?>
				</li>
			<?php } ?>
			
			<?php if($phone_number = get_field('phone_number')){ ?>
				<li>
					<span class="strong"><?php _e('Phone number', 'vektor'); ?>:</span>
					<?php echo $phone_number; ?>
				</li>
			<?php } ?>
			
			<?php if($website = get_field('website')){ ?>
				<li>
					<span class="strong"><?php _e('Website', 'vektor'); ?>:</span>
					<a href="http://<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
				</li>
			<?php } ?>
				
		</ul> 
		
		<ul class="medium-4 columns companyinfo">
				
			<?php if($visiting_address = get_field('visiting_address')){ ?>
				<li>
					<span class="strong"><?php _e('Visiting address', 'vektor'); ?>:</span>
					<?php echo $visiting_address; ?>
				</li>
			<?php } ?>
			
			<?php if($postal_address = get_field('postal_address')){ ?>
				<li>
					<span class="strong"><?php _e('Postal address', 'vektor'); ?>:</span>
					<?php echo $postal_address; ?>
				</li>
			<?php } ?>
		
		</ul> 
	
	</div> <!-- /.row -->
	
	<!--<a href="<?php the_permalink(); ?>" class="strong read-more-link">Läs mer om <?php the_title(); ?> »</a>-->
	
	<?php if (get_field('membership') == 'true') { ?>
		<div class="member-sigill">
			<img src="<?php echo get_template_directory_uri(); ?>/lib/img/medlem-sigill.png" alt="Medlem i SvenskFactoring" />
		</div>
	<?php } ?>
	
		
		<?php /* <div class="large-11 medium-10 small-9 columns company">
			
			<div class="row">
			
				<div class="large-6 medium-5 columns">
				
					<h2 class="h6 company-name"><?php the_title(); ?></h2>
					
					<?php 
						$field = get_field_object('factoring_type');
						$value = get_field('factoring_type');
						$label = $field['choices'][ $value ];
					?>
					
					<ul>
						<?php if(get_field('factoring_type')) {?>
						
							<li class="factoring-type strong"><?php echo $label; ?></li>
							
						<?php }?>
						
						<?php if($our_services_are = get_field('our_services_are')){ ?>
							<li>
								<span class="strong"><?php _e('Our services', 'vektor'); ?>:</span>
								<?php echo implode($our_services_are, ', '); ?>
							</li>
	
						<?php } ?>	
					</ul>
					
					<?php if (get_field('membership') == 'true') { ?>
					
						<!--<span class="toggle-company-info"><span class=""></span><?php echo $btn_text_full_info; ?></span>-->
						
					<?php } ?>
					
				</div>
		
				<ul class="large-4 medium-4 columns website">
				
					<?php if($visiting_address = get_field('visiting_address')){ ?>
						<li>
							<span class="strong"><?php _e('Visiting address', 'vektor'); ?>:</span>
							<?php echo $visiting_address; ?>
						</li>
					<?php } ?>
					
					<?php if($postal_address = get_field('postal_address')){ ?>
						<li>
							<span class="strong"><?php _e('Postal address', 'vektor'); ?>:</span>
							<?php echo $postal_address; ?>
						</li>
					<?php } ?>
					
					<?php if ($email = get_field('email')){ ?>
						<li>
							<span class="strong"><?php _e('Email', 'vektor'); ?>:</span>
							<?php echo $email; ?>
						</li>
					<?php } ?>
					
					<?php if($phone_number = get_field('phone_number')){ ?>
						<li>
							<span class="strong"><?php _e('Phone number', 'vektor'); ?>:</span>
							<?php echo $phone_number; ?>
						</li>
					<?php } ?>
					
					<?php if($website = get_field('website')){ ?>
						<li>
							<span class="strong"><?php _e('Website', 'vektor'); ?>:</span>
							<a href="http://<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
						</li>
					<?php } ?>
				
				</ul>
				
				<ul class="large-2 medium-3 columns">
					
					<?php if (get_field('membership') == 'true') { ?>
						<span class="member-sigill"></span>
					<? } ?>
					
				</ul>
			
			</div> <!-- /.row -->
			
			<div class="row full-company-info">
																					
				<div class="large-12 columns company-profile-link">
		
					<a class="btn" href="<?php the_permalink(); ?>"><?php echo $btn_text_single_company; ?></a>
			
				</div> <!-- /.company-profile-link -->
			
			</div> <!-- /.full-company-info -->
			
		</div> <!-- /.medium-10 --> */ ?>
		
	<!--</div> <!-- /.row -->
	
</article>