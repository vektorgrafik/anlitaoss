<?php
$meta = get_field_objects();
?>


<div class="column">
	
	<div class="result__item">

		<?php if( $meta['company_image']['value'] ) : ?>
		<div class="result__image" style="background-image:url(<?=$meta['company_image']['value']?>);"></div>
		<?php endif; ?>

		<div class="result__content">
			
			<h2 class="h5 result__company-heading">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php if( isset( $meta['area']['value'] )) : ?>
				<span class="result__area"><?=$meta['area']['value']?></span>
				<?php endif; ?>
			</h2>
			
			<ul class="list list--contact-details">
				
				<?php if( $meta['contact_person']['value'] ) : ?>
				<li class="list__item list__item--contact-person"><?=$meta['contact_person']['value']?></li>
				<?php endif;?>
				
				<?php if( $meta['email']['value'] ) : ?>
				<li class="list__item list__item--mail"><a href="mailto:<?=$meta['email']['value']?>"><?=$meta['email']['value']?></a></li>
				<?php endif; ?>

				<?php if( $meta['phone_number']['value'] ) : ?>
				<li class="list__item list__item--phone"><a href="tel:<?=$meta['phone_number']['value']?>"><?=$meta['phone_number']['value']?></a></li>
				<?php endif; ?>
			</ul>
			<a href="<?php the_permalink(); ?>" class="btn btn-small">Mer info <span class="btn__icon"><i class="icon-arrow-right smaller"></i></span></a>
		</div>
	</div>

</div> 