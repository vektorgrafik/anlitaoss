<?php
/*
 * This template gets fed with a few variables from custom.php
 *
 * $total_pages
 * $current_page
 * $results_per_page
 * $num_hits
 * 
 */

$prev_hidden = ( $current_page == 1 );
$next_hidden = ( $total_pages == $current_page );
?>

<?php if( $total_pages > 1 ) : ?>
<nav class="paging">
	
	<?php if( ! $prev_hidden ) : ?>
	<a href="#" data-page="<?=$current_page-1?>" class="paging__item paging__item--prev"><</a>
	<?php endif; ?>
	
	<?php $z = 1; while( $z <= $total_pages ) : $is_active = ( $z == $current_page ) ? 'is-active' : null; ?>
	<a href="#" data-page="<?=$z?>" class="paging__item <?=$is_active?>"><?=$z?></a>
	<?php $z++; endwhile; ?>
	
	<?php if( ! $next_hidden ) : ?>
	<a href="#" data-page="<?=$current_page+1?>" class="paging__item paging__item--next">></a>
	<?php endif; ?>

</nav>
<?php endif; ?>