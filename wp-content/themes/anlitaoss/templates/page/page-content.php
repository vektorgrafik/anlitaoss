<section class="page-content">

	<div class="row">
	
		<?php if(vektor_menu()){ ?>
<!--	SIDEBAR MENY	-->
			<aside class="<?php echo $sidebar_class; ?> columns sidebar">
			
				<?php echo vektor_menu(); ?>
				
			</aside>
			
		<?php } ?>
		
		<div class="large-8 large-offset-2 medium-8 medium-offset-2 small-12 columns content">
		
			<div class="entry-content">
			
				<?php the_content(); ?>
				
			</div> <!-- /.entry-content -->
			
		</div> <!-- /.medium-12 -->
		
	</div> <!-- /.row -->
	
</section>