<section class="section-about space large">

	<div class="row">
	
		<div class="small-12 medium-8 medium-offset-2 large-8 large-offset-2 columns content">
		
			<div class="entry-content text-white">
			
				<?php the_content(); ?>
				
			</div> <!-- /.entry-content -->
			
		</div> <!-- /.medium-12 -->
		
	</div> <!-- /.row -->
	
</section>