<section class="page-content about-us">

	<div class="row">
	
		<div class="small-12 medium-8 medium-centered large-uncentered large-5 columns content">
		
			<div class="entry-content">
			
				<?php the_content(); ?>
				
			</div> <!-- /.entry-content -->
			
		</div>

		<div class="small-12 medium-8 medium-centered large-uncentered large-6 columns">
			
			<div class="contact-info">
				<img src="<?php the_field('contacts_image');?>" class="shadow">

				<div class="row">

					<?php if( have_rows( 'contacts_repeater' )) while( have_rows( 'contacts_repeater' )) : the_row(); ?>
						<div class="small-12 medium-6 columns">
							<ul>
								<li><strong><?php the_sub_field( 'contact_name' ); ?></strong></li>
								<li><strong>Tel: </strong><?php the_sub_field( 'contact_phone' ); ?></li>
								<li><strong>E-post: </strong><?php the_sub_field( 'contact_email' ); ?></li>
							</ul>
						</div>
					<?php endwhile; ?>

				</div>
			</div>

		</div>
		
	</div> <!-- /.row -->
	
</section>