<?php
$bgImage 	= get_field( 'background_image' );
$title 		= get_field( 'page_header_title' );
$preamble 	= get_field( 'page_header_preamble' );
?>


<section class="top-section" style="background-image: url(<?=$bgImage?>);">
	
	<div class="section-darkener"></div>

	<div class="holder">
	
		<div class="content">
		
			<div class="row">
			
				<div class="medium-12 columns medium-centered top-section-content">
				
					<?php if($title){ ?>
					
						<h1 class="h1 video-title"><?php echo $title; ?></h1>
						
					<?php } ?>
					
					<div class="video-text">
					
						<?php if($preamble){ ?>
							<?php echo $preamble; ?>
						<?php } ?>
						
					</div> <!-- /.video-text -->
				
				</div> <!-- /.medium-12 -->
				
			</div> <!-- /.row -->
			
		</div> <!-- /.content -->
		
	</div> <!-- /.holder -->

</section> <!-- /.bgvideo -->