<?php //sidebar
	$sidebar = get_field('sidebar');

	if($sidebar){
		$content_class = "medium-8";
		$sidebar_class = "medium-4";
	} else {
		$content_class = "medium-12";
		$sidebar_class = "";
	}
?>

<section class="page-content">
	<div class="row">
		<div class="<?php echo $content_class; ?> columns content">
			<div class="entry-content">
				<?php the_content(); ?>
			</div> <!-- /.entry-content -->
		</div>
		<?php if($sidebar){ ?>
			<aside class="<?php echo $sidebar_class; ?> columns sidebar">
				<?php while(has_sub_field('sidebar')){ ?>
					<div class="sidebar-content">
						<?php 
							$title = get_sub_field('title');
							$text = get_sub_field('content');
						?>
						<?php if($title){ ?>
							<h3 class="h5"><?php echo $title; ?></h3>
						<?php } ?>
						<?php if($text){ ?>
							<?php echo $text; ?>
						<?php } ?>
					</div> <!-- /.sidebar-content -->
				<?php } ?>
			</aside>
		<?php } ?>
	</div> <!-- /.row -->
</section>