<section class="page-content bg-pattern space large">

	<div class="row">
	
		<div class="large-8 large-offset-2 medium-8 medium-offset-2 small-12 columns content">
		
			<div class="text-center">
			
				<h2 class="divider h3 color-primary"><?php the_field( 'info_header' );?></h2>
				<?php the_field( 'info_body' ); ?>
				
			</div> <!-- /.entry-content -->
			
		</div> <!-- /.medium-12 -->
		
	</div> <!-- /.row -->
	
</section>