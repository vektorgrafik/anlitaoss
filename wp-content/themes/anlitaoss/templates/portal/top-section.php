<?php

$bgImage = get_field( 'portal_background_image' ) ?? null;

$portals = get_posts([
	'post_type' => 'portal',
	'posts_per_page' => -1
]);

?>
<section class="top-section" style="background-image: url(<?=$bgImage?>);">
	
	<div class="content">
	
		<div class="row">
		
			<div class="medium-12 columns medium-centered">
			
				<?php if($title = get_field('portal_header')) { ?>
				
					<h1 class="h1 video-title"><?php echo $title; ?></h1>
					
				<?php } ?>
				
				<div class="video-text">
				
					<?php if( $preamble = get_field( 'portal_preamble' )) {
						echo $preamble;
					 } ?>
					
				</div> <!-- /.video-text -->
				
				<div class="portals">
					
					<p class="uppercase">
						Välj ditt område
					</p>

					<nav class="portal-nav">
						<?php
						foreach( $portals as $portal ) : 

							$name 	= get_field( 'portal_name', $portal->ID );
							$url 	= get_field( 'portal_url', $portal->ID );

							echo '<a class="intro-cta btn" href="' . $url . '">' . $name . '<span class="btn__icon"><i class="icon-arrow-right"></i></span></a>';

						endforeach;
						?>
					</nav>

				</div>

			</div> <!-- /.medium-12 -->
			
		</div> <!-- /.row -->
		
	</div> <!-- /.content -->
		
</section> <!-- /.bgvideo -->