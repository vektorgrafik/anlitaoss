<?php

$factoring_companies_btn_text = get_field('factoring_companies_btn_text');

$factoring_companies_btn_url = get_field('factoring_companies_btn_url');

?>

<section id="factoring-companies" class="section white text-center">
	
	<div class="row">
		
		<div class="medium-9 columns medium-centered">
			
			<?php if($factoring_companies_title = get_field('factoring_companies_title')){ ?>
				
				<h2 class="h3"><?php echo $factoring_companies_title; ?></h2>
			
			<?php }?>
			
			<?php if($factoring_companies_text = get_field('factoring_companies_text')){ ?>
				
				<p class="intro"><?php echo stripp($factoring_companies_text); ?></p>
				
			<?php } ?>
			
		</div> <!-- /.medium-9 -->
		
		<?php $wp_query = new WP_Query(array('post_type' => 'companies', 'meta_key' => 'membership', 'meta_value' => 'true', 'order' => 'ASC', 'posts_per_page' => -1)); ?>
		<div id="carousel-wrapper">
			<div id="carousel">
				<?php while($wp_query->have_posts()): $wp_query->the_post(); ?>
					<?php 
						$logo = get_field('logo');
						$alt = get_the_title($logo['id']);
						$logo = wp_get_attachment_image_src($logo['id'], 'company-logo'); ?>
					
					<?php if($logo){ ?>
						<div>
							<a href="<?php the_permalink(); ?>"><img src="<?php echo $logo[0]; ?>" class="logo" alt="<?php echo $alt;?>"></a>
						</div>
					<?php } ?>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</div> <!-- /#carousel -->
			<a href="#next" id="next" class="next arrow arrow-right"></a>
			<a href="#prev" id="prev" class="prev arrow arrow-left"></a>
		</div> <!-- /#carosuel-wrapper -->
		
		<?php if($factoring_companies_btn_text && $factoring_companies_btn_url){ ?>
			
			<a href="<?php echo $factoring_companies_btn_url; ?>" class="btn btn-blue">
				<?php echo $factoring_companies_btn_text; ?> &raquo;
			</a>
		
		<?php } ?>
	
	</div> <!-- /.row -->
	
</section>