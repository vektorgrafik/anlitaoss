<?php
	
	/*
	 * News
	 */
	
	$news_title = get_field('news_title');
	
	$news_text = get_field('news_text');
	
	$news_btn_text = get_field('news_btn_text');
	
	$news_btn_url = get_field('news_btn_url');
	
?>

<section id="posts" class="lightblue table">

	<div id="news" class="large-4 table-cell space">
		
		<?php if($news_title){ ?>
		
			<h3 class="h4">Nyheter</h3>
		
		<?php } ?>
		
		<?php if($news_text){ ?>
		
			<p class="intro strong"><?php echo $news_text; ?></p>
		
		<?php } ?>
		
		<?php $wp_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 2)); ?>
		
		<?php if($wp_query->have_posts()): ?>
		
			<div class="clickable-rows">
			
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
				
					<article class="clickable">
					
						<h2 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<p><?php echo limit_words(get_the_excerpt(), 12); ?> <a class="readmore" href="<?php the_permalink(); ?>"><?php _e('Continued', 'vektor'); ?> »</a></p>
						
					</article>
					
				<?php endwhile; ?>
				
				<?php wp_reset_query(); ?>
				
			</div> <!-- /.clickable-rows -->
			
			<?php if($news_btn_text){ ?>
			
				<a href="<?php echo $news_btn_url; ?>" class="btn"> <?php echo $news_btn_text; ?> </a>

			<?php } ?>
			
		<?php endif; ?>
		
	</div> <!-- /#posts -->

<?php
	
	/*
	 * Jobs
	 */
	
	$jobs_title = get_field('jobs_title');
	
	$jobs_text = get_field('jobs_text');
	
	$jobs_btn_text = get_field('jobs_btn_text');
	
	$jobs_btn_link = get_field('jobs_btn_link');
	
?>

	<div id="jobs" class="large-4 table-cell space">
	
		<?php if($jobs_title){ ?>
	
			<h3 class="h4"><?php echo $jobs_title; ?></h3>
		
		<?php } ?>
		
		<?php if($jobs_text){ ?>
		
			<p class="intro strong"><?php echo $jobs_text; ?></p>
		
		<?php } ?>
		
		<?php $wp_query = new WP_Query(array('post_type' => 'jobs', 'posts_per_page' => 2)); ?>
		
		<?php if($wp_query->have_posts()): ?>
		
			<div class="clickable-rows">
			
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
				
					<article class="clickable">
					
						<h2 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<p><?php echo limit_words(get_the_excerpt(), 12); ?> <a class="readmore" href="<?php the_permalink(); ?>"><?php _e('Continued', 'vektor'); ?> »</a></p>
						
					</article>
					
				<?php endwhile; ?>
				
				<?php wp_reset_query(); ?>
				
			</div> <!-- /.clickable-rows -->
			
		<?php endif; ?>
		
		<?php if($jobs_btn_text){ ?>
		
			<a href="<?php echo $jobs_btn_link; ?>" class="btn"><?php echo $jobs_btn_text; ?></a>
		
		<?php } ?>
		
	</div> <!-- /#jobs -->

<?php
	
	/*
	 * Purpose
	 */
	
	$purpose_title = get_field('purpose_title');
	
	$purpose_text = get_field('purpose_text');
	
	$purpose_btn_text = get_field('purpose_btn_text');
	
	$purpose_btn_url = get_field('purpose_btn_url');

?>

	<div id="purpose" class="large-4 table-cell space">
		
		<?php if($purpose_title){ ?>
		
			<h3 class="h4"><?php echo $purpose_title; ?></h3>
		
			<?php if($purpose_text){ ?>
				
				<div class="entry-content">
				
					<?php echo $purpose_text; ?>
					
					<?php if($purpose_btn_text && $purpose_btn_url){ ?>
			
				<a href="<?php echo $purpose_btn_url; ?>" class="btn"><?php echo $purpose_btn_text; ?></a>
		
			<?php } ?>
			
				</div> <!-- /.entry-content -->
			
			<?php } ?>
		
		<?php } ?>
		
	</div> <!-- /#purpose -->
	
</section>