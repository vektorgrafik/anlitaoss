<?php

$market_text = get_field('market_text');

$market_btn_text = get_field('market_btn_text');

$market_btn_url = get_field('market_btn_url');

$factoring_in_sweden_title = get_field('factoring_in_sweden_title');

$factoring_in_sweden_text = get_field('factoring_in_sweden_text');

$factoring_in_sweden_btn_text = get_field('factoring_in_sweden_btn_text');

$factoring_in_sweden_btn_url = get_field('factoring_in_sweden_btn_url');

?>

<section id="factoring-in-sweden" class="gray factoring-in-sweden" data-appear-top-offset="-400" data-scroll-offset="77">

	<div class="row">
	
		<div class="large-6 medium-5 columns space">
		
			<div class="stats clearfix" data-stats-max="40000">
			
				<?php while(has_sub_field('market_stats')){ ?>
				
					<div class="bar">
					
						<div class="fill">
							<?php if($value = get_sub_field('value')){ ?>
							
								<div class="number"><?php echo $value; ?></div>
								
							<?php } ?>
							
						</div> <!-- /.fill -->
						
						<?php if($year = get_sub_field('year')){ ?>
						
							<div class="year"><?php echo $year; ?></div>
							
						<?php } ?>
						
					</div> <!-- /.bar -->
					
				<?php } ?>
				
			</div> <!-- /.stats -->
			
			<div class="stats_text">
				<?php if($market_text){ ?>
				
					<?php echo $market_text; ?>
					
				<?php } ?>
			</div>
			
			<?php /* if($market_btn_text && $market_btn_url){ ?>
			
				<a href="<?php echo $market_btn_url; ?>" class="btn btn-yellow"><?php echo $market_btn_text; ?> &raquo;</a>
				
			<?php }*/ ?>
			
		</div> <!-- /.medium-6 -->
		
		<div class="large-6 medium-7 columns space">
		
			<?php if($factoring_in_sweden_title){ ?>
			
				<h2><?php echo $factoring_in_sweden_title; ?></h2>
				
			<?php } ?>
			
			<?php if($factoring_in_sweden_text){ ?>
			
				<?php echo $factoring_in_sweden_text; ?>
				
			<?php } ?>
			
		</div> <!-- /.medium-6 -->
		
	</div> <!-- /.row -->
	
	<div class="row">
	
			<div class="medium-12 medium-centered columns btn-factoring_in_sweden">
		<?php if($factoring_in_sweden_btn_text && $factoring_in_sweden_btn_url){ ?>
		
			<a href="<?php echo $factoring_in_sweden_btn_url; ?>" class="btn btn-yellow btn-factoring_in_sweden"><?php echo $factoring_in_sweden_btn_text; ?> &raquo;</a>
			
		<?php } ?>

		</div>
	
	</div>
	
</section>