<?php

	$main_services = get_terms('company_service', [
		'orderby' 		=> 'name',
		'hide_empty' 	=> false,
		'parent' 		=> 0
	]);

?>

<input type="hidden" name="geo_lat" id="geo_lat">
<input type="hidden" name="geo_lng" id="geo_lng">
<input type="hidden" name="geo_valid" id="geo_valid" value="0">

<section id="choose-main-service">
	<div class="row">
		<div class="medium-12 column text-align-center">
			<?php if ( $section_title = get_field('section_service-title') ) : ?>
				<h2 class="main-service__title h3 divider"><?php echo $section_title; ?></h2>
			<?php endif; ?>
		</div>
	</div>
	<div class="row small-up-1 medium-up-2 large-up-4 small-collapse medium-uncollapse large-uncollapse">
		
		<?php
		foreach( $main_services as $service ) :
			
			if( get_field( 'category_hidden', $service->taxonomy . '_' . $service->term_taxonomy_id)) continue;
		
		?>

			<div class="column service-item">
				<input type="radio" class="service-checkbox <?=$service->slug?>" name="main-service" value="<?=$service->term_id?>" id="main-service-<?=$service->slug?>" data-service="<?=$service->slug?>" data-count="<?=$service->count?>">
				<label for="main-service-<?=$service->slug?>" data-service="<?=$service->slug?>">
					<?php 
						$icon_svg = get_field('icon_svg', 'company_service_' . $service->term_id );
						if ($icon_svg) {
							echo '<span class="icon-wrap">';
							echo '<img src="' . $icon_svg['url'] . '" alt="' . $icon_svg['alt'] . '" class="service-icon"/>';
							echo '</span>';
						}
					?>
					<span class="h6 main-service-caption"><?=$service->name?></span>
				</label>
				
			</div>

		<?php endforeach; ?>

	</div>
	
	<?php if ( $splash_link = get_field('section_service-splash-link') ) : ?>

		<a class="services-splash" href="<?php echo $splash_link; ?>">
			<?php if ( $splash_content = get_field('section_service-splash-content') ) { echo $splash_content; } ?>
		</a>

	<?php endif; ?>

</section>