<section id="choose-city-container" class="grad-bg" style="background: url(http://montagejakt.vglabs.se/media/city3.jpg); background-repeat: none;">
	
	<div class="row">
		<div class="medium-12 text-align-center">
			<h2 class="h5 divider">Vart behöver du hjälp?</h2>
		</div>
	</div>

	<div class="row">
		<div class="medium-8 medium-push-2 text-align-center">
			<div class="picker-wrapper">
				<input type="hidden" name="geo_lat" id="geo_lat">
				<input type="hidden" name="geo_lng" id="geo_lng">
				<a href="#" id="getGeo" class="main-action" title="Vi kan hjälpa dig att hitta din närmsta stad, och även ge dig avstånd till de företag som är närmast dig."><i class="fa fa-fw fa-crosshairs"></i> Hitta företag nära mig</a>
				<a href="#" id="pickCity" class="main-action" title="Välj stad själv."><i class="fa fa-fw fa-building"></i> Välj stad manuellt</a>
			</div>
			<div class="choose-city-wrap">
				<div class="tt-wrapper">
					<input id="choose-city" type="text" placeholder="Fyll i din stad här" class="input-large-white" name="city"></input>	
				</div>
			</div>
		</div>
	</div>

</section>