<section id="section-about" class="section-about space large text-center">

	<div class="row">

		<div class="small-12 medium-8 medium-centered column">
			<?php if ( $section_title = get_field('section_about-title', 4)) : ?>
				<h2 class="section-about__title h3 divider"><?php echo $section_title; ?></h2>
			<?php endif; ?>

			<?php if ( $section_content = get_field('section_about-content', 4)) { echo $section_content; } ?>

			<a href="<?=get_permalink( 88 );?>" class="btn btn-large">Läs mer om oss <span class="btn__icon"><i class="icon-arrow-right"></i></span></a>

		</div>

	</div> <!-- end .row -->

</section>