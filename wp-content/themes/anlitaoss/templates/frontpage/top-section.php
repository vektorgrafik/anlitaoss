<?php

	if ( function_exists('get_field') ) {

		$video_link_mp4 = get_field('video_link_mp4');
		$video_link_webm = get_field('video_link_webm');

		$video_title = get_field('video_title');
		$video_text = get_field('video_text');
		$video_button = get_field('video_btn');

		if ( $video_button ) {

			if ( $video_button['btn_type'] == 'page' ) {
				$button_link = $video_button['btn_page_link'];

			} else {
				//assume it's an anchor link
				$button_link = $video_button['btn_anchor_link'];

			}

			$video_button = '<a href="' . $button_link . '" class="intro-cta btn">' . $video_button['btn_text'] . '<span class="btn__icon"><i class="icon-arrow-down"></i></span></a>';

		}

	}

?>
<section class="bgvideo top-section">
	
	<div class="section-darkener"></div>

	<video class="top-section-background-video" poster="<?=get_template_directory_uri() . '/lib/img/video_bg.jpg'?>" autoplay muted loop>
		<source id="mp4_src" src="<?=$video_link_mp4?>" type="video/mp4">
		<source id="webm_src" src="<?=$video_link_webm?>" type="video/webm">
	</video>

	<div class="holder">
	
		<div class="content">
		
			<div class="row">
			
				<div class="medium-12 columns medium-centered">
				
					<?php if($video_title){ ?>
					
						<h1 class="h1 video-title"><?php echo $video_title; ?></h1>
						
					<?php } ?>
					
					<div class="video-text">
					
						<?php if($video_text){ ?>
							<?php echo $video_text; ?>
						<?php } ?>
						
					</div> <!-- /.video-text -->
					
					<?php if($video_button){ ?>
							<?php echo $video_button; ?>
					<?php } ?>

					
				</div> <!-- /.medium-12 -->
				
			</div> <!-- /.row -->
			
		</div> <!-- /.content -->
		
	</div> <!-- /.holder -->

</section> <!-- /.bgvideo -->