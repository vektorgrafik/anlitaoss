<section id="sok" class="page-content search-content">
	<div class="row">
		<div class="medium-13 columns white">
			<div class="medium-12 columns content">

				<?php get_search_form(); ?>

				<!-- Medlemmar -->
				<?php 

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$search_query_vars = array(
	/*'post_type' => 'companies',
						'meta_key' => 'membership',
						'orderby' => 'meta_value',
						'order' => 'DESC',
						'paged' => $paged,
						'posts_per_page' => 10*/

	'post_type' => 'companies',
	'meta_key' => 'membership',
	'meta_value' => 'true',
	'orderby' => 'title',
	'order' => 'ASC',
	'paged' => $paged,
	'posts_per_page' => 10
);

add_filter('posts_orderby', 'vektor_custom_order');

$wp_query = new WP_Query($search_query_vars);

remove_filter('posts_orderby', 'vektor_custom_order');

if ($wp_query->have_posts()) { ?>

				<h2 class="h5">Medlemmar</h2>
				<?php while($wp_query->have_posts()): $wp_query->the_post(); ?>	

				<?php get_template_part('templates/loop/company'); ?>

				<?php endwhile; ?>
				<?php } ?>	


				<!-- Icke medlemmar -->
				<?php 

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$search_query_vars = array(
	/*'post_type' => 'companies',
						'meta_key' => 'membership',
						'orderby' => 'meta_value',
						'order' => 'DESC',
						'paged' => $paged,
						'posts_per_page' => 10*/

	'post_type' => 'companies',
	'meta_key' => 'membership',
	'meta_value' => 'false',
	'orderby' => 'title',
	'order' => 'ASC',
	'paged' => $paged,
	'posts_per_page' => 21
);

add_filter('posts_orderby', 'vektor_custom_order');

$wp_query = new WP_Query($search_query_vars);

remove_filter('posts_orderby', 'vektor_custom_order');
				?>

			</div>
		</div>
	</div>
</section>	