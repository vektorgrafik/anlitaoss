<section id="results" class="results">
	
	<div class="results__header">

		<div class="row">

			<div class="results__service-name small-12 large-7 column"><h2 class="h3 results__heading">Byggnation</h2></div>
			<div class="results__service-sort small-12 large-5 column">

				<a href="#" id="filter-gps" class="btn btn-uppercase">Nära mig <span class="btn__icon"><i class="icon-map-pin"></i></span></a>
				<a href="#" id="filter-postal" class="btn btn-uppercase"><span class="postal-number">Postnummer</span> <span class="btn__icon"><i class="icon-hash smaller"></i></span></a>

			</div>

		</div> <!-- end .row -->

		<div class="postal-number-wrap">
			<form action="#" onSubmit="return false;">
				<h5 class="text-white body-font" style="display: inline-block;">Fyll i ditt postnummer: </h5>
				<input type="text" name="zip-code" id="zip-code" class="zip-code-input" placeholder="xxxxx" autocomplete="off">
				<a href="#" id="find-by-zip" class="btn btn-large">Sök hantverkare <span class="btn__icon"><i class="icon-arrow-right"></i></span></a>
				<a href="#" class="close-modal">X</a>
			</form>
		</div>

	</div> <!-- end .results__header -->


	<div class="outer-results-wrap">
		<div class="results-preloader">
			<div class="spinner">
			  <div class="dot1"></div>
			  <div class="dot2"></div>
			</div>
		</div>

		<div class="row error-message-wrap">
			<div class="small-12">
				<div class="error-message text-center">
					Vi kunde tyvärr inte hitta din position, men du kan fortfarande filtrera på postnummer.
				</div>
			</div>
		</div>

		<div class="row results__wrap">

			<div class="small-12 large-6 column result__items"></div>

			<div class="small-12 large-6 column">
				
				<div id="google_map"></div>

			</div>

		</div>

	</div>

</section>
</form>