<?php

/*
 * Login styles
 */

function vektor_login_styles() {
	echo '<link rel="stylesheet" href="' . get_stylesheet_directory_uri() . '/lib/css/login.css">';
}

add_action('login_head', 'vektor_login_styles');

/*
 * Login scripts
 */

function vektor_login_scripts() {
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/lib/js/modernizr.min.js', false, null, false );
}

add_action( 'login_enqueue_scripts', 'vektor_login_scripts', 10 );

/*
 * Login url
 */

function vektor_login_url() {
	return home_url();
}

add_filter('login_headerurl', 'vektor_login_url');

/*
 * Login title
 */

function vektor_login_title() {
	return get_option('blogname');
}

add_filter('login_headertitle', 'vektor_login_title');

/*
 * Admin footer
 */

function vektor_vektor_admin_footer() {
	echo '<span id="footer-thankyou">Website by <a href="http://www.vektorgrafik.se" target="_blank">Vektorgrafik</a></span>';
}
add_filter('admin_footer_text', 'vektor_vektor_admin_footer');


/*
 * Allow SVG to media upload
 */

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/** Adds LAT/LNG postmeta to company fields on save */

function add_company_lat_lng( $post_id ) {
    
	global $wpdb;

	$post = get_post( $post_id );

    // bail early if no ACF data
    if( $post->post_type !== 'companies' || empty($_POST['acf']) ) {
        return;
    }
    
	// array of field values
	$fields = $_POST['acf'];

	// specific field value
	$lat = $_POST['acf']['field_5326c6098cf4b']['lat'];
	$lng = $_POST['acf']['field_5326c6098cf4b']['lng'];

	# Clean up previous values
	$wpdb->delete('companies_gps', array( 'post_id' => $post_id ));

	// Also store service-taxes
	
	$tax_list = "";
	if( isset( $_POST['tax_input']['company_service'] ) && is_array( $_POST['tax_input']['company_service'] )) {

		$taxes = $_POST['tax_input']['company_service'];

		// Shift first value since it ain't bein' effin' used.
		array_shift( $taxes );

		$tax_list = implode("|", $taxes );

	}

	# Insert new data
	$wpdb->insert(
		'companies_gps',
		array( 
			'post_id' 	=> $post_id,
			'lat' 		=> $lat,
			'lng' 		=> $lng,
			'tax_list' 	=> '|' . $tax_list . '|'
		), 
		array( 
			'%d',
			'%s',
			'%s',
			'%s'
		) 
	);
}

// run before ACF saves the $_POST['acf'] data
add_action('acf/save_post', 'add_company_lat_lng', 1);



// Remove gps-data from gps-table on post delete
function remove_gps_data( $postid ){

    // We check if the global post type isn't ours and just return
    global $post_type, $wpdb;
    if ( $post_type !== 'companies' ) return;

    $wpdb->delete('companies_gps', array('post_id' => $postid));

}
add_action('before_delete_post', 'remove_gps_data');