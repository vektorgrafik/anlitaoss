<?php

if(!defined('MAINTENANCE_MODE') || MAINTENANCE_MODE !== true)
	return;

function maintenance_mode() {
	if(isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) {
		$ip_addresses = array('85.24.171.11');
		if(in_array($_SERVER['REMOTE_ADDR'], $ip_addresses)) return;
	}
	
	if(is_user_logged_in() || is_admin())
		return;
	
	global $pagenow;
	if(in_array($pagenow, array('wp-login.php', 'wp-signup.php')))
		return;
	
	$maintenance_mode_filename = 'maintenance.php';
	$maintenance_mode_file = get_template_directory() . '/' . $maintenance_mode_filename;
	
	if(file_exists($maintenance_mode_file) && is_readable($maintenance_mode_file)) {
		if(!headers_sent()) {
			nocache_headers();
			
			if(function_exists('http_response_code'))
				http_response_code(503);
			else
				header('HTTP/1.1 503 Service Unavailable');
		}
		
		include $maintenance_mode_file;
		exit;
	} else {
		wp_die(__('Vektor Maintenance Mode active', 'vektor'), get_bloginfo('blogname') . ' - ' . __('Maintenance Mode', 'vektor'));
	}
}
add_action('init', 'maintenance_mode', 1);