<?php

if(!class_exists('Vektor_Menu')) :

class Vektor_Menu {
	
	private $output = '';
	
	function __construct($options = array()) {
		global $post;
		
		$options = wp_parse_args($options, array(
			'post'				=> false,
			'exclude'			=> array(),
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC',
			'show_on_cpt'		=> true,
			'title_in_list'		=> true,
			'title'				=> true,
			'link_title'		=> true,
			'title_before_tag'	=> '<h3>',
			'title_after_tag'	=> '</h3>'
		));
		
		if($options['post']) {
			$post = get_post($options['post']);
		}
		
		$exclude_list = wp_parse_args($options['exclude']);
		
		if(is_search() || is_404())
			return FALSE;
		
		$is_cpt = !get_post_type_object($post->post_type)->_builtin;
		if(!get_post_type_object($post->post_type)->hierarchical)
			return FALSE;
		
		if(is_array($options['show_on_cpt'])) {
			if(!in_array($post->post_type, $options['show_on_cpt']))
				return FALSE;
		} else if(!$options['show_on_cpt']) {
			if($is_cpt)
				return FALSE;
		}
		
		if(is_home()) {
			if($posts_page = get_option('page_for_posts'))
				$post = get_page($posts_page);
			else
				return FALSE;
		}
		
		$post_ancestors = get_post_ancestors($post->ID);
		$top_page = $post_ancestors ? end($post_ancestors) : $post->ID;
		
		$always_excluded = array_merge($post_ancestors, array($post->ID));
		
		$depth = count($post_ancestors) + 1;
		
		foreach($post_ancestors as $post_ancestor) {
			$pages = get_pages(array(
				'child_of'	=> $post_ancestor,
				'parent'	=> $post_ancestor,
				'exclude'	=> $always_excluded,
				'post_type'	=> $post->post_type
			));
			foreach($pages as $page) {
				$excludes = get_pages(array(
					'child_of'	=> $page->ID,
					'parent'	=> $page->ID,
				'post_type'	=> $post->post_type
				));
				foreach($excludes as $exclude) {
					$exclude_list[] = $exclude->ID;
				}
			}
		}
		
		$list_pages_args = array(
			'title_li'		=> '',
			'echo'			=> 0,
			'child_of'		=> $top_page,
			'exclude'		=> implode(',', $exclude_list),
			'depth'			=> $depth,
			'sort_column'	=> $options['orderby'],
			'sort_order'	=> $options['order'],
			'post_type'		=> $post->post_type
		);
		
		if($is_cpt && count($exclude_list) < 1) {
			$list_pages_args = wp_parse_args(array(
				'child_of'	=> 0,
				'depth'		=> 0
			), $list_pages_args);
		}
		
		$children = wp_list_pages($list_pages_args);
		
		if(!$children)
			return FALSE;
			
		if($options['title_in_list']) {
			
			$title = wp_list_pages(array(
				'title_li'	=> '',
				'echo'		=> 0,
				'include'	=> $top_page
			));
			$children = $title . $children;
			
		} else if($options['title']) {
			
			$title = is_string($options['title']) ? apply_filters('the_title', $options['title']) : apply_filters('the_title', get_the_title($top_page));
			$this->output .= $options['title_before_tag'];
			
			if($options['link_title'])
				$this->output .= sprintf('<a href="%s">%s</a>', is_string($options['link_title']) ? esc_attr($options['link_title']) : get_permalink($post), $title);
			else
				$this->output .= $title;
			
			$this->output .= $options['title_after_tag']. "\n";
			
		}
		
		$this->output .= "<ul>\n";
		$this->output .= "\t" . $children . "\n";
		$this->output .= "</ul>\n";
		
		wp_reset_postdata();
	}
	
	function output() {
		return $this->output;
	}
}

if(!function_exists('vektor_menu')) :

function vektor_menu($options = array()) {
	$vektor_menu = new Vektor_Menu($options);
	return $vektor_menu->output();
}

endif;

endif;

