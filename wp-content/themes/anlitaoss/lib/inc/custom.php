<?php
if (!session_id())
    session_start();


add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyBKyZtuo1WDjohf5zAuM-3-77070TFaZyA';
});

/*
* Add your own php functions here
*/

function is_localhost() {
	
	if( $_SERVER['HTTP_HOST'] == 'localhost') {
		return true;
	}

	if( $_SERVER["REMOTE_ADDR"] == '::1' ) {
		return true;
	}

	return false;

}

function is_dev() {
	
	if( $_SERVER['HTTP_HOST'] == '89.221.253.98') {
		return true;
	}

	return is_localhost();

}

function get_visitor_loc_from_ip() {

	$ip = "";
    if (empty($ip)) {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
        else {
	        $ip = $_SERVER['REMOTE_ADDR'];

	        // Local debugging purposes
	        $ip = "176.10.194.135";
	    }
    }
    elseif (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost') {
        $ip = '8.8.8.8';
    }

    $transient = 'ip_loc_' . str_replace('.', '_', $ip);

    //write_log($transient);

	if ( false === ( $loc = get_transient( $transient ) ) ) {

		$res = wp_remote_get("http://ipinfo.io/{$ip}/json");

		if(!is_wp_error($res)) {
			$body = wp_remote_retrieve_body($res);
			$data = json_decode($body);
			$loc = $data->loc;
			set_transient( $transient, $loc, DAY_IN_SECONDS );
		}
	}

	//write_log($loc);
	$loc = str_replace(" ", "", $loc);
	$loc = explode(",", $loc);

	$ret["lat"] = (float) $loc[0];
	$ret["lng"] = (float) $loc[1];

	return $ret;
}

function nice_distance( $distance ) {
	if( $distance < 1 ) {
		return round(($distance * 1000)) . 'm';
	} else {
		return round($distance, 1) . 'km';
	}
}

function calculate_distance($loc1, $loc2, $earthRadius = 6371000)
{
	// convert from degrees to radians
	$latFrom = deg2rad($loc1["lat"]);
	$lonFrom = deg2rad($loc1["lng"]);
	$latTo = deg2rad($loc2["lat"]);
	$lonTo = deg2rad($loc2["lng"]);

	$latDelta = $latTo - $latFrom;
	$lonDelta = $lonTo - $lonFrom;

	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

	return ($angle * $earthRadius) / 1000;
}

function vektor_get_sub_services() {
    
    $term_id = (int) $_GET["term_id"];
    $city 	 = (isset($_GET["city"])) ? esc_attr($_GET["city"]) : null;

	$sub_services = get_terms('company_service', array(
		'orderby' 		=> 'name',
		'hide_empty' 	=> false,
		'parent' 		=> $term_id
	));

	/* Kollar om tjänsten faktiskt har några företag knutna till sig i vald stad */
	if( $city ) {
		foreach( $sub_services as &$service ) {
			$args = array(
				'post_type' => 'companies',
				'orderby'	=> 'name', 
				'order'		=> 'ASC',
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' 	=> 'city',
						'field' 	=> 'name',
						'terms' 	=> array( $city )
					),
					array(
						'taxonomy' 	=> 'company_service',
						'field' 	=> 'id',
						'terms' 	=> array( $service->term_id )
					)
				)
			);
			$companies = get_posts($args);
			$service->exists = ! empty($companies);
		}

		wp_send_json($sub_services);
	    exit;

	} else {
		vektor_get_closest_cities();
	}
}
add_action( 'wp_ajax_get_sub_services', 'vektor_get_sub_services' );
add_action( 'wp_ajax_nopriv_get_sub_services', 'vektor_get_sub_services' );

function store_zip() {

	global $wpdb;

	$zip = (int) $_POST['zip'];
	$lat = $_POST['lat'];
	$lng = $_POST['lng'];

	$wpdb->insert(
		'zip_gps',
		[
			'zip_code' => $zip,
			'lat' => $lat,
			'lng' => $lng
		],
		[
			'%d',
			'%f',
			'%f'
		]
	);

}
add_action( 'wp_ajax_store_zip', 'store_zip' );
add_action( 'wp_ajax_nopriv_store_zip', 'store_zip' );

function get_zip_gps() {

	global $wpdb;

	$zip = (int) $_POST['zip'];

	$response = $wpdb->get_results( $wpdb->prepare(
		'select `lat`, `lng` from zip_gps where zip_code = %d',
		$_POST['zip']
	));

	if( $response ) {
		wp_send_json_success( $response[0] );
	} else {
		wp_send_json_error( 'not in cache' );
	}

}
add_action( 'wp_ajax_get_zip_gps', 'get_zip_gps' );
add_action( 'wp_ajax_nopriv_get_zip_gps', 'get_zip_gps' );

function vektor_get_closest_companies() {

	global $wpdb;

	// Declare / instatiate function vars
	$results_per_page = 5;

	$lat 			= $_POST["lat"] ?? false;
	$lng 			= $_POST["lng"] ?? false;
	$term_id 		= $_POST["term_id"] ?? false;
	$alphabetical	= $_POST["alphabetical"] ?? false;

	$page 			= $_POST['page'] ?? 1;
	$page 			= ( $page == 0 ) ? 1 : $page;
	
	$offset 		= ( $page == 1 ) ? 0 : ( $results_per_page * ( $page - 1 ));
	
	$item_index 	= ( $offset + 1 );
	$start_index 	= ( $offset + 1 );

	$like_clause = "%|" . $term_id . "|%";


	// Default view
	if( $alphabetical ) : 

		// Fetch records without geo
		$posts_array = $wpdb->get_results($wpdb->prepare(
			"select p.ID,
			pm.`lat`,
			pm.`lng`
			from companies_gps pm
			inner join wp_posts p on p.`ID` = pm.`post_id`
			where pm.`tax_list` like %s
			and p.post_status = 'publish'
			order by p.post_title asc limit %d,%d",
			$like_clause,
			$offset,
			$results_per_page
		));

	// GEO-view
	else :

		// Fetch records WITH geo
		$posts_array = $wpdb->get_results($wpdb->prepare(
			"select p.ID,
			pm.`lat`,
			pm.`lng`,
			( 6371 * acos( cos( radians(%f) ) * cos( radians( pm.lat ) ) * cos( radians( pm.lng ) - radians(%f) ) + sin( radians(%f) ) * sin(radians(pm.lat)) ) ) AS distance
			from companies_gps pm
			inner join wp_posts p on p.`ID` = pm.`post_id`
			where pm.`tax_list` like %s
			and p.post_status = 'publish'
			order by distance asc limit %d,%d",
			$lat,
			$lng,
			$lat,
			$like_clause,
			$offset,
			$results_per_page
		));

	endif;

	// Fetch affected rows
	$num_hits = $wpdb->get_var($wpdb->prepare(
		"select count(post_id) as items from companies_gps pm inner join wp_posts p on p.`ID` = pm.`post_id` where `tax_list` like %s and p.post_status = 'publish'",
		$like_clause
	));

	// Start output buffer
	ob_start();

	// Calculate paging
	$total_pages 	= ceil( $num_hits / $results_per_page );
	$current_page 	= $page;

	// Setup some output vars
	$html = "";
	$locations = [];
	$tooltips = [];

	// Fetch result template and setup location data
	$i = 1;
	foreach ( $posts_array as &$post ) {
		
		// Fetch basic data
		$post->title = get_the_title( $post->ID );
		$post->meta = get_field_objects( $post->ID );

		// Add GEO-data to response for use with Google Maps
		array_push( $locations, [
			$post->title,
			$post->lat,
			$post->lng,
			$i
		]);

		// Define tooltip element
		$tooltip = '<div class="content"><h5>' . $post->title . '</h5><a class="btn btn-small" href="' . get_permalink( $post->ID ) . '">Visa företagsprofil <span class="btn__icon"><i class="icon-arrow-right smaller"></i></span> </a></div>';

		// Push current tooltip object to response array
		array_push( $tooltips, $tooltip );

		// Fetch output template
		include get_template_directory() . '/templates/loop/result__item.php';

		// Increment external pointers
		$item_index++;
		$i++;

	}

	// Attach paging template
	include get_template_directory() . '/templates/parts/paging.php';

	// Collect buffer
	$html = ob_get_clean();

	// Setup response data
	$response_data = [
		'html' => $html,
		'tooltips' => $tooltips,
		'geo' => $locations,
		'total_pages' => $total_pages,
		'current_page' => $current_page,
		'offset' => $start_index
	];

	// Store entire response in session.
	//$_SESSION['latest_response'] = $response_data;

	// Return data
	wp_send_json_success( $response_data );

}
add_action( 'wp_ajax_get_closest_companies', 'vektor_get_closest_companies' );
add_action( 'wp_ajax_nopriv_get_closest_companies', 'vektor_get_closest_companies' );

function get_latest_response() {

	$response = $_SESSION['latest_response'] ?? false;

	if( $response && !empty( $response['geo'] )) {
		wp_send_json_success( $response );
	} else {
		wp_send_json_error( 'no data' );
	}

}
add_action( 'wp_ajax_get_latest_response', 'get_latest_response' );
add_action( 'wp_ajax_nopriv_get_latest_response', 'get_latest_response' );

function make_rating($no) {
	if( (int) $no == 0) return '';

	$ret = "";
	$x = 0;
	$no = (int) $no;

	while ($x < $no) {
		$ret .= '<i class="fa fa-fw fa-star" style="color: gold"></i>';
		$x++;
	}
	while ($x < 5) {
		$ret .= '<i class="fa fa-fw fa-star-o" style="color: #ccc"></i>';
		$x++;
	}

	return $ret;
}


/*
	Hämta resultat baserat på input i frontend.
*/
function vektor_get_results() {
    
    $city 			= $_GET["city"];
    $service_id 	= (int) $_GET["sub-service"];

    $loc["lat"]		= (float) $_GET["geo_lat"];
    $loc["lng"] 	= (float) $_GET["geo_lng"];

    $sort_column 	= (isset($_GET["sort_column"])) ? $_GET["sort_column"] : null;
    $sort_order 	= (isset($_GET["sort_order"])) ? $_GET["sort_order"] : null;

    if( isset( $_SESSION["geo_post_ids"])) {
		$args = array(
			'post_type' => 'companies',
			'posts_per_page' => -1,
			'post__in' => $_SESSION["geo_post_ids"],
			'tax_query' => array(
				array(
					'taxonomy' 	=> 'company_service',
					'field'		=> 'id',
					'terms' 	=> array($service_id)
				)
			)
		);
    } else {
		$args = array(
			'post_type' => 'companies',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' 	=> 'city',
					'field' 	=> 'name',
					'terms' 	=> array( esc_attr($city) )
				),
				array(
					'taxonomy' 	=> 'company_service',
					'field'		=> 'id',
					'terms' 	=> array($service_id)
				)
			)
		);
    }


	$posts = get_posts( $args );

	$data = array();
	/* Fetch ACF fields and sort return by lowest price */
	foreach( $posts as $post ) {
		$cur = array();
		
		$cur["company_name"] 		 	= $post->post_title;
		$cur["company_logo"] 		 	= get_field('logo', $post->ID);
		$cur["company_presentation"] 	= get_field('company_presentation', $post->ID);
		$cur["company_orgnr"] 		 	= get_field('organization_number', $post->ID);
		
		$cur["company_address"] 	 	= get_field('visiting_address', $post->ID);
		$cur["company_postal_address"] 	= get_field('postal_address', $post->ID);
		
		$cur["company_phone"] 			= get_field('phone_number', $post->ID);
		$cur["company_phone_link"] 		= preg_replace("/[^0-9]/","",$cur["company_phone"]);
		
		$cur["company_email"] 			= get_field('email', $post->ID);
		$cur["company_website_href"] 	= get_field('website', $post->ID);
		$cur["company_website"] 		= str_replace("http://", "", $cur["company_website_href"]);

		$cur["post_id"]				 	= $post->ID;
		$cur["permalink"]			 	= get_permalink($post->ID);

		$cur["rating"] 					= make_rating(get_field('rating', $post->ID));

		if( isset($_SESSION["geo_distances"])) {
			$cur["raw_distance"] = (int) $_SESSION["geo_distances"][$post->ID];
			$cur["distance"] = nice_distance($_SESSION["geo_distances"][$post->ID]);
		}

		if( have_rows('service_repeater', $post->ID)) while( have_rows('service_repeater', $post->ID)) : the_row();
			
			$acf_service_id = get_sub_field('service', $post->ID);

			// Hårt och brutalt på första indexet. Man får inte ha fler än EN.
			if( $acf_service_id == $service_id ) {
				$cur["price"] = get_sub_field('price');
				$cur["unit"] = get_sub_field('enhet');
				break;
			}
		endwhile;
		array_push($data, $cur);
	}

/*	var_dump( $data );
	exit;*/

	# Sort by distance
	if( $sort_column == "distance" ) {
		usort($data, function($a, $b) {
	    	return $a["raw_distance"] - $b["raw_distance"];
		});		
	}

	# Sort by price
	if( $sort_column == "price" || $sort_column == null ) {
		usort($data, function($a, $b) {
	    	return $a["price"] - $b["price"];
		});		
	}

	if( $sort_order == "desc" ) {
		$data = array_reverse($data);
	}

	wp_send_json($data);
    exit;
}
add_action( 'wp_ajax_get_results', 'vektor_get_results' );
add_action( 'wp_ajax_nopriv_get_results', 'vektor_get_results' );


// Get city list
function vektor_generate_citylist() {
	
	$cities = get_terms('city', array(
		'orderby' => 'name',
		'hide_empty' => true
	));
	$output = array();
	foreach ( $cities as $city ) {
		array_push($output, $city->name);
	}
	wp_localize_script( 'app', 'cities', $output);
}
//add_action('wp_enqueue_scripts', 'vektor_generate_citylist', 200);


function vektor_prevent($prevent, $query) {
	return false;
}

// Används i template-factoring-companies.php
function vektor_custom_order($orderby_statement) {
	global $wpdb;
	
	$orderby_statement .= ", {$wpdb->posts}.post_title ASC";
	return $orderby_statement;
}

/*
 * Limit words of a string
 */

function limit_words($string, $word_limit) {
	$words = explode(' ', $string, ($word_limit + 1));
	if(count($words) > $word_limit)
	array_pop($words);
	return implode(' ', $words);
}

/*
 * Get custom taxonomies
 */

function get_custom_tax($tax){
	global $post;
	$terms = get_the_terms($post->ID, $tax);
	if($terms){
		$out = array();
		foreach($terms as $term){
			$out[] = $term->name;
		}
		$out = implode($out, ', ');
		return $out;
	} else {
		return null;
	}
}


if(function_exists('acf_add_options_sub_page')):

function vektor_register_option_sub_pages(){
    acf_add_options_sub_page(array(
        'title' => 'Temainställningar',
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));
}
add_action( 'init', 'vektor_register_option_sub_pages');

endif;