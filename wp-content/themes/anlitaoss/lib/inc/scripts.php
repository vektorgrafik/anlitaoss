<?php

/*
 * Enqueue scripts and styles
 */

function vektor_scripts() {

	/*
	 * Register styles
	 */
	wp_register_style( 'vektor', get_template_directory_uri() . '/lib/css/vektor.css', false, '1.0', false );
	wp_register_style( 'plugins', get_template_directory_uri() . '/lib/css/plugins.css', false, '1.0', false );
	wp_register_style( 'style', get_template_directory_uri() . '/style.css', false, '1.0', false );

	/*
	 * Enqueue styles
	 */

	wp_enqueue_style('vektor');
	wp_enqueue_style('plugins');
	wp_enqueue_style('style');

	/*
	 * Register scripts
	 */

	// Modernizr
	wp_register_script( 'modernizr', get_template_directory_uri() . '/lib/js/modernizr.min.js', false, null, false );

	// jQuery
	wp_deregister_script('jquery');

	wp_register_script( 'jquery', get_template_directory_uri() . '/lib/js/jquery.min.js', false, null, true );
	wp_register_script( 'plugins', get_template_directory_uri() . '/lib/js/plugins.js', false, '1.0', true );
	wp_register_script( 'app', get_template_directory_uri() . '/lib/js/app.js', false, '1.2', true );

	// Vektor object
	wp_localize_script( 'app', 'vektor', array(
		'themepath' => get_template_directory_uri(),
		'ajaxurl' 	=> admin_url( 'admin-ajax.php' )
	));

	/*
	 * Enqueue scripts
	 */

	wp_enqueue_script('modernizr');
	wp_enqueue_script('jquery');
	wp_enqueue_script('smoothscroll');
	wp_enqueue_script('plugins');
	wp_enqueue_script('app');
	
}

add_action( 'wp_enqueue_scripts', 'vektor_scripts', 100 );

/*
 * IE Scripts
 */

function ie_scripts(){
	echo '<!--[if lt IE 9]>
	<script src="' . get_template_directory_uri() . '/lib/js/respond.min.js"></script>
	<![endif]-->';
}

add_action('wp_head', 'ie_scripts');

/*
 * Google Analytics
 */

function vektor_google_analytics() { ?>
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','<?php echo GOOGLE_ANALYTICS_ID; ?>');ga('send','pageview');
</script>

<?php }
if (GOOGLE_ANALYTICS_ID && !current_user_can('manage_options')) {
  add_action('wp_footer', 'vektor_google_analytics', 20);
}