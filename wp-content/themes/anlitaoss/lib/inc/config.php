<?php

/*
 * Configuration values
 */

$google_analytics = get_option('vektor_google_analytics') ? get_option('vektor_google_analytics') : '';
$excerpt_length = get_option('vektor_excerpt') ? get_option('vektor_excerpt') : 50;

define('GOOGLE_ANALYTICS_ID', $google_analytics); // UA-XXXXX-Y
define('POST_EXCERPT_LENGTH', $excerpt_length); // length in words for excerpt_length filter (ref: http://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_length)