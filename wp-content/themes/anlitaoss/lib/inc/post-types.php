<?php

/*
Custom Post Types

This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'vektor_flush_rewrite_rules' );

// Flush your rewrite rules
function vektor_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function vektor_post_types() { 

	// creating (registering) the custom type 
	register_post_type( 'companies',
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Companies', 'vektor' ),
			'singular_name' => __( 'Company', 'vektor' ),
			'all_items' => __( 'Show all', 'vektor' ),
			'add_new' => __( 'Add new', 'vektor' ),
			'add_new_item' => __( 'Add new', 'vektor' ),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __( 'Edit', 'vektor' ),
			'new_item' => __( 'New', 'vektor' ),
			'view_item' => __( 'View', 'vektor' ),
			'search_items' => __( 'Search', 'vektor' ),
			'not_found' =>  __( 'Nothing found in the Database.', 'vektor' ),
			'not_found_in_trash' => __( 'Nothing found in Trash', 'vektor' ),
			'parent_item_colon' => ''
			),
			'description' => __( '', 'vektor' ),
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-building',
			'rewrite'	=> array( 'slug' => 'foretag', 'with_front' => false ),
			'has_archive' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
		
			'supports' => array( 'title', 'editor', 'thumbnail')
		)
	); /* end of register post type */
	
	// creating (registering) the custom type 
	register_post_type( 'portal', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Portaler', 'vektor' ),
			'singular_name' => __( 'Portal', 'vektor' ),
			'all_items' => __( 'Show all', 'vektor' ),
			'add_new' => __( 'Add new', 'vektor' ),
			'add_new_item' => __( 'Add new', 'vektor' ),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __( 'Edit', 'vektor' ),
			'new_item' => __( 'New', 'vektor' ),
			'view_item' => __( 'View', 'vektor' ),
			'search_items' => __( 'Search', 'vektor' ),
			'not_found' =>  __( 'Nothing found in the Database.', 'vektor' ),
			'not_found_in_trash' => __( 'Nothing found in Trash', 'vektor' ),
			'parent_item_colon' => ''
			),
			'description' => __( '', 'vektor' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7,
			'menu_icon' => 'dashicons-admin-site',
			'rewrite'	=> array( 'slug' => 'site', 'with_front' => false ),
			'has_archive' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
		
			'supports' => array( 'title', 'editor', 'thumbnail')
		) /* end of options */
	); /* end of register post type */

	register_taxonomy('company_service', 'companies', array(
		'label' => 'Tjänster',
		'hierarchical' => true,
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true
		//'rewrite' => array('slug' => 'tjanst')
	));
/*	register_taxonomy('city', 'companies', array(
	    'label' => "Städer",
	    'hierarchical' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'stad' )
	    )
	);*/
}
// adding the function to the Wordpress init
add_action( 'init', 'vektor_post_types');


function remove_custom_taxonomy()
{
    remove_meta_box('company_servicediv', 'companies', 'side' );
}
//add_action( 'admin_menu', 'remove_custom_taxonomy' );

