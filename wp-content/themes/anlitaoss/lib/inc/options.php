<?php

/*
 * Create our admin menu
 */

function vektor_options_create_menu() {

	// create new top-level menu
	add_options_page(__('Other settings', 'vektor'), __('Other settings', 'vektor'), 'administrator', 'vektor-theme', 'vektor_options_settings_page');
}
// create custom plugin settings menu
add_action('admin_menu', 'vektor_options_create_menu');

/*
 * Init admin settings
 */

function vektor_options_admin_init() {

	// register our settings
	register_setting( 'vektor-settings-group', 'vektor_google_analytics' );
	register_setting( 'vektor-settings-group', 'vektor_google_tag_manager' );
	register_setting( 'vektor-settings-group', 'vektor_excerpt' );

	register_setting( 'vektor-settings-group', 'vektor_name' );
	register_setting( 'vektor-settings-group', 'vektor_email' );
	register_setting( 'vektor-settings-group', 'vektor_phone' );
	register_setting( 'vektor-settings-group', 'vektor_address' );
	register_setting( 'vektor-settings-group', 'vektor_zip' );
	register_setting( 'vektor-settings-group', 'vektor_city' );
	register_setting( 'vektor-settings-group', 'vektor_country' );
	register_setting( 'vektor-settings-group', 'vektor_footer_text' );

}
add_action( 'admin_init', 'vektor_options_admin_init' );

function vektor_options_settings_page() {
?>
<div class="wrap">
	<h2><?php echo get_admin_page_title(); ?></h2>
	<form method="post" action="options.php">
		<?php settings_fields( 'vektor-settings-group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><?php _e('Tag Manager Code', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_google_tag_manager" value="<?php echo get_option('vektor_google_tag_manager'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Google Analytics ID', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_google_analytics" value="<?php echo get_option('vektor_google_analytics'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Excerpt length', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_excerpt" value="<?php echo get_option('vektor_excerpt'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><h2>Sidfot</h2></th>
				<td>&nbsp;</td>
			</tr>
			<tr valign="top">
				<th scope="row">Text i sidfoten</th>
				<td><textarea style="width: 100%; padding: 0.5em;" name="vektor_footer_text" /><?php echo get_option('vektor_footer_text'); ?></textarea></td>
			</tr>
			<tr valign="top">
				<th scope="row"><h2><?php _e('Contact details', 'vektor'); ?></h2></th>
				<td>&nbsp;</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Name / company', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_name" value="<?php echo get_option('vektor_name'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Email', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_email" value="<?php echo get_option('vektor_email'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Phone', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_phone" value="<?php echo get_option('vektor_phone'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Address', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_address" value="<?php echo get_option('vektor_address'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Zip', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_zip" value="<?php echo get_option('vektor_zip'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('City', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_city" value="<?php echo get_option('vektor_city'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e('Country', 'vektor'); ?></th>
				<td><input type="text" style="width: 100%; padding: 0.5em;" name="vektor_country" value="<?php echo get_option('vektor_country'); ?>" /></td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
	</form>
</div> <!-- /.wrap -->
<?php } ?>