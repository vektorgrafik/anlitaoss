<?php
/*
 * Template Name: Sidofält
 */
?>

<?php get_header(); ?>
<?php the_post(); ?>

	<?php get_template_part('templates/page/page-header'); ?>
	
	<?php get_template_part('templates/page/page-content-sidebar'); ?>

<?php get_footer(); ?>