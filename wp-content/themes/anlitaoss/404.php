<?php get_header(); ?>
	<section class="page-header page-header-image stellar">
		<div class="row">
			<div class="medium-12 columns">
				<h1><?php _e('404 - Not found', 'vektor'); ?></h1>
			</div> <!-- /.medium-12 -->
		</div> <!-- /.row -->
	</section>
	<section class="section">
		<div class="row">
			<article class="medium-12 columns text-center">
				<p><?php _e('Sorry, the page you are looking for has been moved or permanently deleted.', 'vektor'); ?></p>
			</article>
		</div> <!-- /.row -->
	</section> <!-- /.section -->
<?php get_footer(); ?>