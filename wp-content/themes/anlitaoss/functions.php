<?php

/*
 * Vektor includes
 */

$selectable_files = array(
	'/lib/inc/post-types.php' // Custom post types
);

foreach($selectable_files as $selectable_file)
	locate_template($selectable_file, true);

/*
 * Pagination
 */ 
 
 function kriesi_pagination($pages = '', $range = 2) {  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}


/*
 * Stop edit here!
 */

$required_files = array(
	'utils.php',
	'init.php',
	'config.php',
	'activation.php',
	'cleanup.php',
	'titles.php',
	'nav.php',
	'admin.php',
	'widgets.php',
	'scripts.php',
	'options.php',
	'custom.php'
);
	
foreach($required_files as $required_file)
	locate_template('/lib/inc/' . $required_file, true);

$plugin_files = array(
	'vektor-menu.php', // vektor_menu(), replaces simple_section_nav()
	'vektor-download.php', // get_the_download_attachment_url() and the_download_attachment_url()
	'vektor-maintenance-mode.php', // To activate maintenance mode, set MAINTENANCE_MODE to true in config.php
	'vektor-lang.php', // Easier po/mo language files management
);

foreach($plugin_files as $plugin_file)
	locate_template('/lib/inc/plugins/' . $plugin_file, true);
