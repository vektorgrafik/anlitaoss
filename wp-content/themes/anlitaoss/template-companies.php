<?php
/*
 * Template Name: Företagskatalog
 */

$companies = new Wp_Query([
	'post_type' => 'companies',
	'order' => 'asc',
	'orderby' => 'title',
	'posts_per_page' => -1
]);

?>

<?php get_header(); ?>

<?php the_post(); ?>

	<?php get_template_part('templates/page/page-header'); ?>
	
	<section id="companies" class="page-content">
	
		<div class="row small-up-1 medium-up-1 large-up-2">
			<?php
			if( $companies->have_posts() ) while( $companies->have_posts() ) : $companies->the_post();
				get_template_part( 'templates/loop/company-list-item' );
			endwhile;
			?>
		</div>

	</section>

<?php get_footer(); ?>